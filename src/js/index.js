import "../styles/reset.scss";
import "../styles/style.scss";

(function () {
  const backgroundColor = [
    "white",
    "#a8e6cf",
    "rgb(220, 237, 193)",
    "#ffd3b6",
    "rgb(255, 170, 165)",
    "#5d50c6",
    "blueviolet",
    "#f85e9f",
    "#f18fac",
    "rgb(250, 205, 73)",
  ];
  const backgroundElement = document.body;
  const colorNameElement = document.querySelector("#color-text");
  const clickButton = document.querySelector("button");

  let currentColorIndex = 0;

  function getRandomIndex(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }

  function changeBackgroundColor(newColor) {
    backgroundElement.style.background = newColor;
  }

  function changeColorName(newColor) {
    colorNameElement.textContent = newColor;
  }

  function applyNewColor() {
    const newRandomIndex = getRandomIndex(0, backgroundColor.length);
    if (newRandomIndex === currentColorIndex) {
      return applyNewColor();
    }

    currentColorIndex = newRandomIndex;
    changeBackgroundColor(backgroundColor[currentColorIndex]);
    changeColorName(backgroundColor[currentColorIndex]);
  }

  clickButton.addEventListener("click", applyNewColor);
})();
